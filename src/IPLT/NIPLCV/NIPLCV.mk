##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=NIPLCV
ConfigurationName      :=Release
WorkspacePath          := "/home/charles/Project_Linux/src/IPLT"
ProjectPath            := "/home/charles/Project_Linux/src/IPLT/NIPLCV"
IntermediateDirectory  :=./Release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Charles
Date                   :=20/12/16
CodeLitePath           :="/home/charles/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/lib$(ProjectName).so
Preprocessors          :=$(PreprocessorSwitch)_LINUX 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="NIPLCV.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  -O2
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)/usr/local/include $(IncludeSwitch)$(Product_Include) $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)opencv_core $(LibrarySwitch)opencv_highgui $(LibrarySwitch)opencv_imgcodecs $(LibrarySwitch)opencv_videoio 
ArLibs                 :=  "libopencv_core" "libopencv_highgui" "libopencv_imgcodecs" "libopencv_videoio" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/local/lib 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS := -std=c++14 -std=c++11 -fPIC  $(Preprocessors)
CFLAGS   :=   $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Product_Include:=../../../include
Product_Lib:=../../../lib
Product_Bin:=../../../bin
Objects0=$(IntermediateDirectory)/NIPLCV.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(SharedObjectLinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)
	@$(MakeDirCommand) "/home/charles/Project_Linux/src/IPLT/.build-release"
	@echo rebuilt > "/home/charles/Project_Linux/src/IPLT/.build-release/NIPLCV"

PostBuild:
	@echo Executing Post Build commands ...
	../CopyFiles.sh POST ./Release/libNIPLCV.so
	@echo Done

MakeIntermediateDirs:
	@test -d ./Release || $(MakeDirCommand) ./Release


$(IntermediateDirectory)/.d:
	@test -d ./Release || $(MakeDirCommand) ./Release

PreBuild:
	@echo Executing Pre Build commands ...
	../CopyFiles.sh PRE NIPLCV
	@echo Done


##
## Objects
##
$(IntermediateDirectory)/NIPLCV.cpp$(ObjectSuffix): NIPLCV.cpp $(IntermediateDirectory)/NIPLCV.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/charles/Project_Linux/src/IPLT/NIPLCV/NIPLCV.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/NIPLCV.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/NIPLCV.cpp$(DependSuffix): NIPLCV.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/NIPLCV.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/NIPLCV.cpp$(DependSuffix) -MM "NIPLCV.cpp"

$(IntermediateDirectory)/NIPLCV.cpp$(PreprocessSuffix): NIPLCV.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/NIPLCV.cpp$(PreprocessSuffix) "NIPLCV.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Release/


