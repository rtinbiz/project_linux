#include "NIPLCV.h"

int main(int argc, char** argv)
{
    Mat img = Mat::zeros(200, 200, CV_8UC1);
    namedWindow("original", WINDOW_AUTOSIZE);
    imshow("original", img);

    NIPLInput dInput;
    NIPLOutput dOutput;
    
    NIPLParam_Invert dParam;
    
    dInput.m_pParam = &dParam;
    dInput.m_dImg = img;
    
    NIPLCV *pNIPLCV = NIPLCV::GetInstance();
    NIPL_ERR nErr = pNIPLCV->Invert(&dInput, &dOutput);
    if(NIPL_SUCCESS(nErr)) {
        namedWindow("invert", WINDOW_AUTOSIZE);
        imshow("invert", dOutput.m_dImg);
    }
    
    waitKey(0);

    return 0;
}
