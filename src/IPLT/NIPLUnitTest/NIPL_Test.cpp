#include "stdafx.h"
#include "NIPLUnitTest.h"

TEST(NIPL_Test, CreateImage) {
	NIPL *pNIPL = NIPL::GetInstance();
	NIPLInput dInput;
	NIPLOutput dOutput;
	NIPL_ERR nErr;

	//
	// Fail Case
	//
	// no input argument
	nErr = pNIPL->CreateImage(nullptr, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	// no output argument
	nErr = pNIPL->CreateImage(&dInput, nullptr);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	// no parameter set in input argument
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_NO_PARAMS);

	NIPLParam_CreateImage dParam_CreateImage;
	dInput.m_pParam = &dParam_CreateImage;

	int nWidth = 100;
	int nHeight = 100;
	dParam_CreateImage.m_nType = NIPLParam_CreateImage::TYPE_GRAY_UINT8;
	dParam_CreateImage.m_nWidth = nWidth;
	dParam_CreateImage.m_nHeight = nHeight;
	dParam_CreateImage.m_nValue = 0;

	// Success Cases
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	int nWidthOutputImg = dOutput.m_dImg.cols;
	int nHeightOutputImg = dOutput.m_dImg.rows;
	EXPECT_TRUE(NIPL_SUCCESS(nErr) && nWidth == nWidthOutputImg && nHeight == nHeightOutputImg);

	float nValue = 100.f;
	dParam_CreateImage.m_nValue = nValue;
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	float nMean = static_cast<float>(mean(dOutput.m_dImg)[0]);
	EXPECT_TRUE(NIPL_SUCCESS(nErr) && nMean == nValue);

	// Invalid Case
	dParam_CreateImage.m_nWidth = 0;
	dParam_CreateImage.m_nHeight = 100;
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);

	dParam_CreateImage.m_nWidth = 100;
	dParam_CreateImage.m_nHeight = 0;
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);

	// Extreme Case
	dParam_CreateImage.m_nWidth = 1000000;
	dParam_CreateImage.m_nHeight = 1000000;
	nErr = pNIPL->CreateImage(&dInput, &dOutput);
	EXPECT_TRUE(nErr == NIPL_ERR_INVALID_PARAM_VALUE);
}