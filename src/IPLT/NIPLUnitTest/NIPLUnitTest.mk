##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Release
ProjectName            :=NIPLUnitTest
ConfigurationName      :=Release
WorkspacePath          := "/home/charles/Project_Linux/src/IPLT"
ProjectPath            := "/home/charles/Project_Linux/src/IPLT/NIPLUnitTest"
IntermediateDirectory  :=./Release
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Charles
Date                   :=20/12/16
CodeLitePath           :="/home/charles/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=$(PreprocessorSwitch)_LINUX $(PreprocessorSwitch)NDEBUG 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="NIPLUnitTest.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)/usr/local/include $(IncludeSwitch)$(Product_Include) $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)opencv_core $(LibrarySwitch)opencv_highgui $(LibrarySwitch)opencv_imgcodecs $(LibrarySwitch)opencv_videoio $(LibrarySwitch)NIPL $(LibrarySwitch)NIPLCV $(LibrarySwitch)gtest 
ArLibs                 :=  "libopencv_core" "libopencv_highgui" "libopencv_imgcodecs" "libopencv_videoio" "libNIPL" "libNIPLCV" "libgtest" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/local/lib $(LibraryPathSwitch)$(Product_Lib) 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS := -std=c++14 -std=c++11 -O2 -Wall $(Preprocessors)
CFLAGS   :=  -O2 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Product_Include:=../../../include
Product_Lib:=../../../lib
Product_Bin:=../../../bin
Objects0=$(IntermediateDirectory)/NIPL_Test.cpp$(ObjectSuffix) $(IntermediateDirectory)/NIPLUnitTest.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	./CopyFiles.sh ./Release/NIPLUnitTest
	@echo Done

MakeIntermediateDirs:
	@test -d ./Release || $(MakeDirCommand) ./Release


$(IntermediateDirectory)/.d:
	@test -d ./Release || $(MakeDirCommand) ./Release

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/NIPL_Test.cpp$(ObjectSuffix): NIPL_Test.cpp $(IntermediateDirectory)/NIPL_Test.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/charles/Project_Linux/src/IPLT/NIPLUnitTest/NIPL_Test.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/NIPL_Test.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/NIPL_Test.cpp$(DependSuffix): NIPL_Test.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/NIPL_Test.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/NIPL_Test.cpp$(DependSuffix) -MM "NIPL_Test.cpp"

$(IntermediateDirectory)/NIPL_Test.cpp$(PreprocessSuffix): NIPL_Test.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/NIPL_Test.cpp$(PreprocessSuffix) "NIPL_Test.cpp"

$(IntermediateDirectory)/NIPLUnitTest.cpp$(ObjectSuffix): NIPLUnitTest.cpp $(IntermediateDirectory)/NIPLUnitTest.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/charles/Project_Linux/src/IPLT/NIPLUnitTest/NIPLUnitTest.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/NIPLUnitTest.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/NIPLUnitTest.cpp$(DependSuffix): NIPLUnitTest.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/NIPLUnitTest.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/NIPLUnitTest.cpp$(DependSuffix) -MM "NIPLUnitTest.cpp"

$(IntermediateDirectory)/NIPLUnitTest.cpp$(PreprocessSuffix): NIPLUnitTest.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/NIPLUnitTest.cpp$(PreprocessSuffix) "NIPLUnitTest.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Release/


