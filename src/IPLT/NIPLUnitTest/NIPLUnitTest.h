#pragma once

#include "stdafx.h"
#include "gtest/gtest.h"

using namespace testing;

#ifdef _WINDOWS
#ifdef _DEBUG
#pragma comment (lib, "GTest/gtestd.lib")
#else
#pragma comment (lib, "GTest/gtest.lib")
#endif  // _DEBUG
#endif  // _WINDOWS 